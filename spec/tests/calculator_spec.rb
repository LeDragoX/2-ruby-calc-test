require 'spec_helper'
require_relative "../../src/calculator"

describe Calculator do
    describe ".calcSum" do

        context "Given an empty string" do
          it "Returns zero" do
            expect(Calculator.calcSum("")).to eq(0)
          end
        end

        context "Given 1 String and 1 Integer" do
            it "Returns 7" do
              expect(Calculator.calcSum("5", 2)).to eq(7)
            end
          end
  
          context "Given an null number" do
            it "Returns zero" do
              expect(Calculator.calcSum(nil)).to eq(0)
            end
          end
  
    end
end
