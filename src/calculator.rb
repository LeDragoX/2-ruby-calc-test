class Calculator
    def self.calcSum(n1 = 0, n2 = 0)
        # Do the Sum between 2 numbers
        n1 = n1.to_f
        n2 = n2.to_f
        result = n1+n2
        puts "[Sum] : #{n1} + #{n2} = #{result}"
        return result
    end
    
    def self.calcSub(n1 = 0, n2 = 0)
        # Do the Subtraction between 2 numbers
        n1 = n1.to_f
        n2 = n2.to_f
        result = n1-n2
        puts "[Sub] : #{n1} - #{n2} = #{result}"
        return result
    end
    
    def self.calcMult(n1 = 0, n2 = 0)
        # Do the Multiplication between 2 numbers
        n1 = n1.to_f
        n2 = n2.to_f
        result = n1*n2
        puts "[Mult]: #{n1} x #{n2} = #{result}"
        return result
    end
    
    def self.calcDiv(n1 = 0, n2 = 0)
        # Do the Division between 2 numbers
        n1 = n1.to_f
        n2 = n2.to_f
        result = n1/n2
        puts "[Div] : #{n1} / #{n2} = #{result}"
        return result
    end
    
    def self.calcPow(n1 = 0, n2 = 0)
        # Do the Exponential between 2 numbers
        n1 = n1.to_f
        n2 = n2.to_f
        result = n1**n2
        puts "[Pow] : #{n1} ^ #{n2} = #{result}"
        return result
    end
end

#c1 = Calculator.new()
#c1.calcSum(5, 2)